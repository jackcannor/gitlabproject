package com.example.demo;

import static org.junit.Assert.*;
import org.junit.Test;

import object.MyObject;

public class DemoApplicationTests {

	private MyObject obj = new MyObject(5,2);
	
	@Test
	public void addtest() {
		double db = obj.add(2);
		assertTrue(db == 9);
	}

	@Test
	public void substractTest() {
		double db = obj.substract(2);
		assertTrue(db == 5);
	}

}